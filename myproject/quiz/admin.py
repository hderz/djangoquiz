from django.contrib import admin

from quiz.models import Question, Poll, PollInstance, Topic, QuestionType, Choice, TextAnswer

admin.site.register(Poll)
admin.site.register(PollInstance)
admin.site.register(Topic)
admin.site.register(QuestionType)
admin.site.register(Choice)
admin.site.register(TextAnswer)


class ChoiceInline(admin.StackedInline):
    model = Choice
    extra = 1


class TextFieldInline(admin.StackedInline):
    model = TextAnswer
    extra = 1


class QuestionAdmin(admin.ModelAdmin):
    fieldset = ['poll', 'text', 'right_answer', 'questionType', 'variants']
    inlines = [ChoiceInline, TextFieldInline]

admin.site.register(Question, QuestionAdmin)
