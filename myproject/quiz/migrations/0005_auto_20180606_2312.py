# Generated by Django 2.0.6 on 2018-06-06 20:12

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0004_pollinstance_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='questiontype',
            name='id_type',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='pollinstance',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2018, 6, 6, 23, 12, 51, 164793)),
        ),
    ]
