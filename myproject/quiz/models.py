from django.db import models
from django.contrib.auth.models import User
import jsonfield
import datetime


class Question(models.Model):
    id = models.IntegerField(primary_key=True)
    poll = models.ForeignKey('Poll', on_delete=models.SET_NULL, null=True)
    text = models.TextField(max_length=500, help_text="Enter the question")
    questionType = models.ForeignKey('QuestionType', on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.text


class Topic(models.Model):
    name = models.CharField(max_length=20, help_text="Enter a topic name")

    def __str__(self):
        return self.name


class Poll(models.Model):
    topic = models.ForeignKey('Topic', on_delete=models.SET_NULL, null=True)
    name = models.CharField(max_length=20, help_text="Enter a poll name", null=True)

    def __str__(self):
        return self.name


class PollInstance(models.Model):
    id = models.IntegerField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    poll = models.ForeignKey(Poll, on_delete=models.SET_NULL, null=True)
    result = models.CharField(max_length=20, help_text="Enter the result")
    answers = jsonfield.JSONField(default=dict)
    date = models.DateTimeField(default=datetime.datetime.now())

    def __str__(self):
        return str(self.id)


class QuestionType(models.Model):
    name = models.CharField(max_length=200, null=False)
    id_type = models.CharField(max_length=200, null=True)

    def __str__(self):
        return self.name


class TextAnswer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.SET_NULL, null=True)
    answerText = models.CharField(max_length=20, default='No choice')


class Choice(TextAnswer):
    right_answer = models.BooleanField()
