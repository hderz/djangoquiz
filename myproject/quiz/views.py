# -*- coding: utf-8 -*-
import datetime
from django.contrib.auth.models import User, Group
from django.contrib.auth.hashers import make_password
from django.shortcuts import render
import json

from .models import Topic, Poll, Question, PollInstance, QuestionType, TextAnswer, Choice


def index(request):
    # Генерация "количеств" некоторых главных объектов
    topics = Topic.objects.all()
    topics_name = [x.name for x in topics]

    polls = {}
    for topic in topics:
        polls[topic.name] = Poll.objects.filter(topic=topic).count()

    return render(
        request,
        'index.html',
        context={'polls': polls, 'topics': topics_name},
    )


def poll(request, poll_name):
    poll = Poll.objects.get(name=poll_name)
    questions = Question.objects.filter(poll=poll)
    choices = {}

    for question in questions:
        choices[(question.id, question)] = Choice.objects.filter(question=question)

    # pl = PollInstance.objects.filter(poll=poll)
    users = []

    # for p in pl:
    # users.append(p.user.username)

    return render(request, 'poll.html', context={'poll': poll_name, 'dict_choices': choices, 'users': users})


def calc_result(request, poll_name, user):
    print(poll_name)
    print(request.POST)
    if request.method == "POST":
        poll = Poll.objects.get(name=poll_name)
        questions = Question.objects.filter(poll=poll)

        res = 0
        answers = {}
        for question in questions:
            text = str(question.text)
            answers[text] = []
            if question.questionType.id_type == "question with multiple right answers":
                choices = Choice.objects.filter(question=question)

                flag = True
                for i in range(len(choices)):
                    print(flag)
                    print(choices[i].answerText)
                    try:
                        a = request.POST['choice_box_' + str(question.id) + '_' + str(i)]
                        print(a)
                        if choices[i].right_answer:
                            answers[text].append([a, 'right'])
                        else:
                            flag = False
                            answers[text].append([a, 'wrong'])
                    except KeyError:
                        if choices[i].right_answer:
                            flag = False

                if flag:
                    res += 1

            if question.questionType.id_type == "question with one right answer":
                choices = Choice.objects.filter(question=question)
                for i in range(len(choices)):
                    if choices[i].right_answer:
                        try:
                            a = request.POST['choice_radio_' + str(question.id)]
                            if a == choices[i].answerText:
                                res += 1
                                answers[text].append([a, 'right'])
                            else:
                                answers[text].append([a, 'wrong'])
                        except KeyError:
                            pass

            if question.questionType.id_type == "question with text field":
                choice = TextAnswer.objects.get(question=question)
                a = request.POST['answer_' + str(question.id)].lower()
                if a == choice.answerText.lower():
                    res += 1
                    answers[text].append([a, 'right'])
                else:
                    answers[text].append([a, 'wrong'])

        id = int(len(PollInstance.objects.all())) + 1
        poll_instance = PollInstance()
        poll_instance.id = id
        poll_instance.user = User.objects.get(username=user)
        poll_instance.poll = poll
        poll_instance.result = str(res)
        poll_instance.answers = json.dumps(answers)
        poll_instance.date = datetime.datetime.now()
        print(poll_instance.date)
        poll_instance.save()

        return render(request, 'result.html', context={'answers': answers, 'result': res,
                                                       'qcount': Question.objects.filter(poll=poll).count(),
                                                       'username': user})


def show_poll_instance_res(request, poll_name, user, id):
    poll = Poll.objects.get(name=poll_name)
    pl = PollInstance.objects.get(poll=poll, id=id)
    answers = pl.answers

    return render(request, 'result.html', context={'answers': answers, 'result': pl.result,
                                                   'qcount': Question.objects.filter(poll=poll).count(),
                                                   'username': user})


def display_polls_by_topic(request, topic_name):
    topic = Topic.objects.get(name=topic_name)
    polls = Poll.objects.filter(topic=topic)
    return render(request, 'polls_list.html', context={'polls': polls})


def display_user_polls(request, user):
    u = User.objects.get(username=user)
    polls = PollInstance.objects.filter(user=u)
    data = {}
    for x in polls:
        data[x] = Question.objects.filter(poll = x.poll).count()
    return render(request, 'user_polls.html', context={'username': user, "data":data})


def register(request):
    return render(request, 'registration/register.html', {'error': ""})


def register_success(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        password2 = request.POST['repeat_password']
        error = ""
        if password == password2 and User.objects.filter(username=username).count() == 0:
            user = User()
            user.username = username
            user.password = make_password(password)
            user.save()
            user.groups.add(Group.objects.get(name="student"))
            return render(request, 'registration/success_register.html')
        if password != password2:
            error += "Пароли не совпадают. "
        if User.objects.filter(username=username).count() != 0:
            error += "Такое имя пользователя уже занято. "
        return render(request, 'registration/register.html', {'error': error})


def create_poll(request):
    qtypes = QuestionType.objects.all()
    types = [x.name for x in qtypes]
    types.insert(0, '------------------')
    topics = Topic.objects.all()
    topics_name = [x.name for x in topics]
    return render(request, 'create_poll.html', {'types': types, 'topics': topics_name})


def save_poll(request):
    if request.method == "POST":
        print(request.POST)
        topic = Topic.objects.get(name=request.POST['topic'])
        poll = Poll()
        poll.name = request.POST['pollname']
        poll.topic = topic
        poll.save()
        for x in request.POST.keys():
            if 'questiontype' in x:
                id = x.replace('questiontype', '')
                q = createQuestion(request, id, poll)
                createChoise(request, id, q)
        return render(request, 'success_poll_save.html')


def createQuestion(request, id, poll):
    q = Question()
    q.id = Question.objects.count() + 1
    q.poll = poll
    q.text = request.POST['questiontext' + id]
    q.questionType = QuestionType.objects.get(name=request.POST['questiontype' + id])
    q.save()
    return q


def createChoise(request, id, q):
    if request.POST['questiontype' + id] == 'Вопрос с текстовым полем':
        ta = TextAnswer()
        ta.question = q
        ta.answerText = request.POST['rightanswer' + id]
        ta.save()
    if request.POST['questiontype' + id] == 'Вопрос с одним правильным ответом':

        for i in range(1, 5):
            c = Choice()
            c.question = q
            c.answerText = request.POST['choice' + str(i) + "_" + id]
            if request.POST['choice_radio' + id] == str(i):
                c.right_answer = True
            else:
                c.right_answer = False

            c.save()

    if request.POST['questiontype' + id] == 'Вопрос с несколькими вариантами ответов':

        for i in range(1, 5):
            c = Choice()
            c.question = q
            c.answerText = request.POST['choice' + str(i) + "_" + id]
            try:
                if request.POST['choice_box' + str(i) + "_" + id] == 'on':
                    c.right_answer = True
            except KeyError:
                c.right_answer = False
            c.save()


def studentres(request):
    all_users = User.objects.all()
    students = []
    for u in all_users:
        if u.groups.all()[0].name == "student":
            students.append(u)
    count_of_poll_instance = {}
    for student in students:
        count_of_poll_instance[student] = PollInstance.objects.filter(user=student).count()
    return render(request, 'students.html', {'count_of_poll_instance': count_of_poll_instance})


def changepoll(request, poll_name):
    poll = Poll.objects.get(name=poll_name)
    questions = Question.objects.filter(poll=poll)
    choices = {}
    topics = Topic.objects.all()
    for question in questions:
        if question.questionType.name == "question with text field":
            choices[(question.id, question)] = TextAnswer.objects.filter(question=question)
        else:
            choices[(question.id, question)] = Choice.objects.filter(question=question)

    qtypes = QuestionType.objects.all()
    types = [x.name for x in qtypes]
    types.insert(0, '------------------')
    return render(request, 'change_poll.html',
                  context={'poll': poll, 'dict_choices': choices, 'topics': topics, 'nextID': Question.objects.count(),
                           'types': types})


def change_poll(request):
    if request.method == "POST":
        return render(request, 'success_poll_save.html')
