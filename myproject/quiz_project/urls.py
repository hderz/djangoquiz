"""quiz_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import django.contrib.auth.views as auth

from quiz import views

app_name = 'quiz'

urlpatterns = [

    url(r'^$', views.index, name='home'),
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^accounts/login/$',  auth.login),
    url(r'^register/$', views.register, name='register'),
    url(r'^register/success/$', views.register_success, name='registersuccess'),
    url(r'^poll/(?P<poll_name>[0-9A-Za-z_-]+)/$', views.poll, name='poll'),
    url(r'^poll/result/(?P<poll_name>[0-9A-Za-z_-]+)/(?P<user>[0-9A-Za-z_-]+)/$', views.calc_result, name='pollresult'),
    url(r'^poll/result/(?P<poll_name>[0-9A-Za-z_-]+)/(?P<user>[0-9A-Za-z_-]+)/(?P<id>[0-9]+)/$',
        views.show_poll_instance_res, name='showresult'),
    url(r'^topic/(?P<topic_name>[0-9A-Za-z_-]+)/$', views.display_polls_by_topic, name='pollbytopic'),
    url(r'^profile/mypolls/(?P<user>[0-9A-Za-z_-]+)/&', views.display_user_polls, name='userpolls'),
    url(r'^createpoll/$',views.create_poll, name='createpoll'),
    url(r'^createpoll/save/$', views.save_poll, name='savepollsuccess'),
    url(r'^createpoll/change/$', views.change_poll, name='changepollsuccess'),
    url(r'^studentres/$',views.studentres, name='studentres'),
    url(r'^changepoll/(?P<poll_name>[0-9A-Za-z_-]+)/$', views.changepoll, name='changepoll')
]
